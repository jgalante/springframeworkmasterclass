package com.jgalante.springdemo.domain.product;

public abstract class GenericProduct {
	protected int priceRandomizer = 1000;

	public abstract int calculatePrice();
}
