package com.jgalante.springdemo;

import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class IOCAppChallenge1 {
	
	public static void main(String[] args) throws BeansException, IOException {
		// create the application context (container)
		// note: FileSystemXmlApplicationContext is the IoC implementation 
		ApplicationContext ctx = new FileSystemXmlApplicationContext(new ClassPathResource("beans-challenge.xml").getFile().getAbsolutePath());
		
		// create the bean
		City city = (City) ctx.getBean("mycity");
		
		// invoke the city name method
		city.cityName();
		
		// close the application context (container)
		((FileSystemXmlApplicationContext) ctx).close();		
	}

}
