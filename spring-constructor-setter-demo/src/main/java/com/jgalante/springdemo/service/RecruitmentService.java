package com.jgalante.springdemo.service;

public interface RecruitmentService {
	public String recruitEmployees(String companyName, String departmentName, int numberOfRecruitments);
}
