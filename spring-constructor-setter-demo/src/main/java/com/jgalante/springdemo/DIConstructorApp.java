package com.jgalante.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jgalante.springdemo.domain.HumanResourceDept;
import com.jgalante.springdemo.domain.Organization;

public class DIConstructorApp {
	
	public static void main(String[] args) {
		// create the application context (container)
		// note: ClassPathXmlApplicationContext is the IoC implementation 
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml");
		
		// create the bean
		Organization org = (Organization) ctx.getBean("myorg");
		
		// invoke the company slogan via the bean
		System.out.println(org.corporateSlogan());
		
		// print organization details
//		System.out.println(org);
		
//		System.out.println(org.corporateService());
		HumanResourceDept hrDept = (HumanResourceDept) ctx.getBean("myhrdept");
		System.out.println(hrDept.hiringStatus(5500));
		
		// close the application context (container)
		((ClassPathXmlApplicationContext) ctx).close();		
	}

}
