package com.jgalante.springdemo.domain;

public interface Department {
	public String hiringStatus(int numberOfRecruitments);
}
